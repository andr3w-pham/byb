import "../../styles/FormModal.css";
import { Modal, Col, Row } from "react-bootstrap";
import { FcCheckmark } from "react-icons/fc";
const FormModal = ({ show, handleCloseModal, contacts }) => {
  const {
    firstName,
    lastName,
    accountName,
    companyName,
    phone,
    fax,
    title,
    email,
    emailOptOut,
    streetNumberAndStreet,
    city,
    state,
    postcode,
    description,
  } = contacts;

  return (
    <Modal size="xl" show={show} onHide={handleCloseModal}>
      <Modal.Header className="form-modal-header" closeButton>
        <Modal.Title className="form-modal-title">
          Saved <FcCheckmark className="save-tick" />
        </Modal.Title>
      </Modal.Header>

      <div className="container formal-modal-message">
        <p className="form-modal-text-color">
          The Contact Details Have Been Saved
        </p>
      </div>

      <Modal.Body
        className="form-modal-body
      "
      >
        <h3 className="form-heading-color">Contact Information</h3>
        {/* Contact Information Section */}
        <Row className="mt-4">
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">First Name</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{firstName}</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Last Name</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{lastName}</p>
          </Col>
        </Row>

        <Row>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Account Name</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{accountName}</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Company Name</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{companyName}</p>
          </Col>
        </Row>

        <Row>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Phone</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{phone}</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Fax</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{fax}</p>
          </Col>
        </Row>

        <Row>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Title</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{title}</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Email</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{email}</p>
          </Col>
        </Row>

        <Row>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Email Opt Out</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{emailOptOut}</p>
          </Col>
        </Row>

        {/* Address Information Section */}
        <h3 className="form-heading-color mt-4">Address Information</h3>

        <Row className="mt-4">
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Street NO. & Street</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{streetNumberAndStreet}</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">City</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{city}</p>
          </Col>
        </Row>

        <Row>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">State</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{state}</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color">Postcode</p>
          </Col>
          <Col sm={6} md={6} lg={3}>
            <p>{postcode}</p>
          </Col>
        </Row>

        {/* Description Information Section */}
        <h3 className="form-heading-color mt-4">Description Information</h3>
        <Row className="mt-4">
          <Col sm={6} md={6} lg={3}>
            <p className="form-modal-text-color mb-5">Description</p>
          </Col>
          <Col sm={9} md={9} lg={9}>
            <p>{description}</p>
          </Col>
        </Row>
      </Modal.Body>
    </Modal>
  );
};

export default FormModal;
