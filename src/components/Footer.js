import "../styles/Footer.css";
import { Col, Row, Container } from "react-bootstrap";

const Footer = () => {
  return (
    <Container>
      <footer className="mt-5 font-size-sm">
        {/* Row Start */}
        <Row className="mt-4">
          <Col sm={6} md={6} lg={8}>
            <div>
              <p>
                Copyright &copy; 2020 Crunch Accounting. All rights reserved
              </p>
            </div>
          </Col>
          <Col sm={3} md={3} lg={2}>
            <div>
              <p>Privacy Policy </p>
            </div>
          </Col>
          <Col sm={3} md={3} lg={2}>
            <div>
              <p>Terms of Service</p>
            </div>
          </Col>
        </Row>
        {/* Row End */}
      </footer>
    </Container>
  );
};

export default Footer;
