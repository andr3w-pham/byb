import { Form, InputGroup } from "react-bootstrap";

const TextSelect = ({
  id,
  value,
  onChange,
  name,
  isInvalid,
  options,
  onBlur,
}) => {
  return (
    <InputGroup.Text className="custom-input-group-text" id={id}>
      <Form.Select
        onChange={onChange}
        onBlur={onBlur}
        name={name}
        isInvalid={isInvalid}
        value={value}
      >
        {options?.map((optionSelect) => (
          <option key={optionSelect.id} value={optionSelect.value}>
            {optionSelect.label}
          </option>
        ))}
      </Form.Select>
    </InputGroup.Text>
  );
};

export default TextSelect;
