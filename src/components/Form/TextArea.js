import { Col, Row, Form } from "react-bootstrap";

const TextArea = ({
  controlId,
  name,
  as,
  style,
  value,
  onChange,
  isInvalid,
  errorsDisplay,
  onBlur,
}) => {
  return (
    <Row className="mt-4 mb-2">
      <Col sm={8} md={8} lg={8}>
        <Form.Group controlId={controlId}>
          <Form.Label>Description</Form.Label>
          <Form.Control
            name={name}
            as={as}
            style={style}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            isInvalid={isInvalid}
          />
          <Form.Control.Feedback type="invalid">
            {errorsDisplay}
          </Form.Control.Feedback>
        </Form.Group>
      </Col>
    </Row>
  );
};

export default TextArea;
