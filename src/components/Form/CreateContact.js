import "../../styles/CreateContact.css";
import { Button } from "react-bootstrap";
import logo from "../../assets/images/crunch-accounting-logo.png";
import React from "react";

const CreateContact = ({ onClick, disabled }) => {
  return (
    <React.Fragment>
      <div>
        <img className="center" src={logo} alt="logo" />
      </div>
      <div className="create-contact-bg-color ">
        <h3 className="create-contact-heading">Create Contact</h3>
        <ul className="create-contact-ul">
          <Button
            className="create-contact-cancel-btn"
            type="reset"
            onClick={onClick}
          >
            Cancel
          </Button>
          <Button
            disabled={disabled}
            type="submit"
            className="create-contact-save-btn btn"
          >
            Save
          </Button>
        </ul>
      </div>
    </React.Fragment>
  );
};

export default CreateContact;
