import "../styles/ContactForm.css";
import React from "react";
import CheckBox from "./Form/CheckBox";
import CreateContact from "./Form/CreateContact";
import FormModal from "./Form/FormModal";
import TextArea from "./Form/TextArea";
import TextField from "./Form/TextField";
import TextFieldCol from "./Form/TextFieldCol";
import TextSelect from "./Form/TextSelect";
import { useState } from "react";
import { Formik } from "formik";
import * as Yup from "yup";

import { Col, Form, Row, InputGroup } from "react-bootstrap";

const ContactFormSchema = Yup.object().shape({
  titleFirstName: Yup.string().required(),
  firstName: Yup.string()
    .min(2, "Need at least 2 characters.")
    .matches(/^[aA-zZ\s]+$/, "Only letters are allowed for this field")
    .required("This field is required"),
  lastName: Yup.string()
    .min(2, "Need at least 2 characters")
    .matches(/^[aA-zZ\s]+$/, "Only letters are allowed for this field")
    .required("This field is required"),
  accountName: Yup.string()
    // Regex expression only accepting letters
    .matches(/^[aA-zZ\s]+$/, "Only letters are allowed for this field")
    .required("This field is required"),
  phone: Yup.string()
    // Regex expression only accepting numbers & spaces
    .matches(/^[0-9 ]+$/, "Only numbers are allowed for this field")
    .required("This field is required"),
  email: Yup.string().email("Invalid email").required("This field is required"),
  streetNumberAndStreet: Yup.string().required("This field is required"),
  city: Yup.string()
    .matches(/^[aA-zZ\s]+$/, "Only letters are allowed for this field")
    .required("This field is required"),
  state: Yup.string()
    .matches(/^[aA-zZ\s]+$/, "Only letters are allowed for this field")
    .required("This field is required"),
  postcode: Yup.string()
    // Regex expression only accepting numbers
    .matches(/^[0-9]+$/, "Only numbers are allowed for this field")
    .min(4, "Australian postcode must be 4 numbers, please try again")
    .max(4, "Australian postcode can not exceed 4 numbers, please try again")
    .required("This field is required"),
  description: Yup.string().required("This field is required"),
});

function ContactForm({ options }) {
  const defaultValues = {
    titleFirstName: "",
    firstName: "",
    lastName: "",
    accountName: "",
    companyName: "",
    phone: "",
    fax: "",
    title: "",
    email: "",
    emailOptOut: false,
    streetNumberAndStreet: "",
    city: "",
    state: "",
    postcode: "",
    description: "",
  };

  const [show, setShow] = useState(false);
  const handleCloseModal = () => setShow(false);
  const handleShowModal = () => setShow(true);

  const [contacts, setContacts] = useState(defaultValues);
  const handleFormData = (data) => {
    console.log(data);
    setContacts(data);
    handleShowModal();
  };

  return (
    <React.Fragment>
      <FormModal
        show={show}
        handleCloseModal={handleCloseModal}
        handleShowModal
        contacts={contacts}
      />

      <Formik
        initialValues={defaultValues}
        validationSchema={ContactFormSchema}
        onSubmit={(values) => {
          handleFormData(values);
        }}
      >
        {(formik) => (
          <Form
            className="form-padding"
            noValidate
            onSubmit={formik.handleSubmit}
          >
            <CreateContact
              onClick={formik.resetForm}
              //Disables Save Button, disabled until all required fields are valid. Also checks if the values have been changed from initial values.
              disabled={!(formik.isValid && formik.dirty)}
            />

            <div className="container-fluid">
              {/* Contact  Information Section */}
              <h3 className="form-heading-color">Contact Information</h3>
              {/* Row Start */}
              <Row className="mt-5">
                <Col sm={6} md={4} lg={4}>
                  <Form.Group controlId="firstName">
                    <Form.Label>First Name</Form.Label>

                    <InputGroup className="mb-3">
                      {/* Reusable Component - TextSelect */}
                      <TextSelect
                        value={formik.values.titleFirstName}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        name="titleFirstName"
                        isInvalid={
                          formik.touched.titleFirstName &&
                          formik.errors.titleFirstName
                        }
                        options={options}
                        id={"title-firstName"}
                      />

                      {/* Reusable Component - TextField */}
                      <TextField
                        name="firstName"
                        placeholder="John"
                        value={formik.values.firstName}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        isInvalid={
                          formik.touched.firstName && formik.errors.firstName
                        }
                        type="text"
                      />
                      <Form.Control.Feedback type="invalid">
                        {formik.errors.firstName}
                      </Form.Control.Feedback>
                    </InputGroup>
                  </Form.Group>
                </Col>

                <TextFieldCol
                  controlId="lastName"
                  label="Last Name"
                  name="lastName"
                  type="text"
                  placeholder="Smith"
                  value={formik.values.lastName}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.lastName && formik.errors.lastName}
                  errorsDisplay={formik.errors.lastName}
                />
              </Row>
              {/* Row End */}

              {/* Row Start */}
              <Row className="mt-5">
                <TextFieldCol
                  controlId="accountName"
                  label="Account Name"
                  name="accountName"
                  type="text"
                  placeholder="John's Joinery"
                  value={formik.values.accountName}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={
                    formik.touched.accountName && formik.errors.accountName
                  }
                  errorsDisplay={formik.errors.accountName}
                />

                <TextFieldCol
                  controlId="companyName"
                  label="Company Name (optional)"
                  name="companyName"
                  type="text"
                  value={formik.values.companyName}
                  onChange={formik.handleChange}
                />
              </Row>

              {/* Row End */}

              {/* Row Start */}
              <Row className="mt-5">
                <TextFieldCol
                  controlId="phone"
                  label="Phone"
                  name="phone"
                  type="text"
                  placeholder="02 123 456 78"
                  value={formik.values.phone}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.phone && formik.errors.phone}
                  errorsDisplay={formik.errors.phone}
                />

                <TextFieldCol
                  controlId="fax"
                  label="Fax (optional)"
                  name="fax"
                  type="text"
                  value={formik.values.fax}
                  onChange={formik.handleChange}
                />
              </Row>
              {/* Row End */}

              {/* Row Start */}
              <Row className="mt-5">
                <TextFieldCol
                  controlId="title"
                  label="Title (optional)"
                  name="title"
                  type="text"
                  placeholder="Owner"
                  value={formik.values.title}
                  onChange={formik.handleChange}
                />

                <TextFieldCol
                  controlId="email"
                  label="Email"
                  name="email"
                  type="email"
                  placeholder="sample@email.com"
                  value={formik.values.email}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.email && formik.errors.email}
                  errorsDisplay={formik.errors.email}
                />
              </Row>
              {/* Row End */}

              {/* Row Start */}
              <Row className="mt-5">
                <CheckBox
                  controlId="emailOptOut"
                  label="Email Opt Out"
                  name="emailOptOut"
                  type="checkbox"
                  value={formik.values.emailOptOut}
                  onChange={formik.handleChange}
                />
              </Row>
              {/* Row End */}

              {/* Address  Information Section */}
              <h3 className="form-heading-color mt-5">Address Information</h3>
              {/* Row Start */}
              <Row className="mt-5">
                <TextFieldCol
                  controlId="streetNumberAndStreet"
                  label="Street No. & Street"
                  name="streetNumberAndStreet"
                  type="text"
                  placeholder="1, Elizabeth Street"
                  value={formik.values.streetNumberAndStreet}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={
                    formik.touched.streetNumberAndStreet &&
                    formik.errors.streetNumberAndStreet
                  }
                  errorsDisplay={formik.errors.streetNumberAndStreet}
                />

                <TextFieldCol
                  controlId="city"
                  label="City"
                  name="city"
                  type="text"
                  placeholder="Sydney"
                  value={formik.values.city}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.city && formik.errors.city}
                  errorsDisplay={formik.errors.city}
                />
              </Row>
              {/* Row End */}

              {/* Row Start */}
              <Row className="mt-5">
                <Col sm={6} md={4} lg={4}>
                  <Form.Group controlId="state">
                    <Form.Label>State</Form.Label>
                    <Form.Control
                      list="addressStates"
                      name="state"
                      type="text"
                      placeholder="New South Wales"
                      value={formik.values.state}
                      onChange={formik.handleChange}
                      onBlur={formik.handleBlur}
                      isInvalid={formik.touched.state && formik.errors.state}
                    />
                    <datalist id="addressStates">
                      <option value="New South Wales" />
                      <option value="Victora" />
                      <option value="Queensland" />
                      <option value="Western Australia" />
                      <option value="South Australia" />
                      <option value="Tasmania" />
                    </datalist>

                    <Form.Control.Feedback type="invalid">
                      {formik.errors.state}
                    </Form.Control.Feedback>
                  </Form.Group>
                </Col>

                <TextFieldCol
                  controlId="postcode"
                  label="PostCode"
                  name="postcode"
                  type="text"
                  placeholder="2000"
                  value={formik.values.postcode}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  isInvalid={formik.touched.postcode && formik.errors.postcode}
                  errorsDisplay={formik.errors.postcode}
                />
              </Row>
              {/* Row End */}
              {/* Description Information Section */}
              <h3 className="form-heading-color mt-5">
                Description Information
              </h3>
              {/* Row Start */}
              <TextArea
                controlId="description"
                name="description"
                as="textarea"
                style={{ height: "250px" }}
                value={formik.values.description}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                isInvalid={
                  formik.touched.description && formik.errors.description
                }
                errorsDisplay={formik.errors.description}
              />
              {/* Row End */}
            </div>
          </Form>
        )}
      </Formik>
    </React.Fragment>
  );
}

export default ContactForm;
