import { FormControl } from "react-bootstrap";

const TextField = ({
  value,
  onChange,
  name,
  placeholder,
  isInvalid,
  type,
  onBlur,
}) => {
  return (
    <FormControl
      name={name}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      onBlur={onBlur}
      isInvalid={isInvalid}
      type={type}
    />
  );
};

export default TextField;
