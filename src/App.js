import "./styles/App.css";
import ContactForm from "./components/ContactForm";
import Footer from "./components/Footer";

function App() {
  const titleFirstNameOptions = [
    { id: 1, label: "- None", value: "" },
    { id: 2, label: "Mr", value: "mr" },
    { id: 3, label: "Mrs", value: "mrs" },
    { id: 4, label: "Miss", value: "miss" },
    { id: 5, label: "Ms", value: "ms" },
    { id: 6, label: "Dr", value: "dr" },
  ];

  return (
    <div className="App">
      <ContactForm options={titleFirstNameOptions} />
      <Footer />
    </div>
  );
}

export default App;
