import { Col, Form } from "react-bootstrap";

const CheckBox = ({ controlId, label, name, type, value, onChange }) => {
  return (
    <Col sm={4} md={4} lg={4}>
      <Form.Group className="mb-3" controlId={controlId}>
        <Form.Check
          label={label}
          name={name}
          type={type}
          value={value}
          onChange={onChange}
        />
      </Form.Group>
    </Col>
  );
};

export default CheckBox;
