import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import ContactForm from "../components/ContactForm";

describe("ContactForm", () => {
  it("firstName input validation check", async () => {
    render(<ContactForm />);
    const firstNameInput = screen.getByPlaceholderText("John");
    userEvent.type(firstNameInput, "123");
    await screen.findByText("Only letters are allowed for this field");
  });

  it("lastName input validation check", async () => {
    render(<ContactForm />);
    const lastNameInput = screen.getByRole("textbox", {
      name: /last name/i,
    });
    userEvent.type(lastNameInput, "Bob123");
    await screen.findByText("Only letters are allowed for this field");

    userEvent.clear(lastNameInput);
    userEvent.type(lastNameInput, "a");
    await screen.findByText("Need at least 2 characters");
  });

  it("accountName input validation check", async () => {
    render(<ContactForm />);
    const accountNameInput = screen.getByRole("textbox", {
      name: /account name/i,
    });
    userEvent.type(accountNameInput, "123account4567");
    await screen.findByText("Only letters are allowed for this field");
  });

  it("phone input validation check", async () => {
    render(<ContactForm />);
    const phoneInput = screen.getByRole("textbox", {
      name: /phone/i,
    });
    userEvent.type(phoneInput, "phone1234567890");
    await screen.findByText("Only numbers are allowed for this field");
  });

  it("email input validation check", async () => {
    render(<ContactForm />);
    const emailInput = screen.getByRole("textbox", {
      name: /email/i,
    });
    userEvent.type(emailInput, "test");
    await screen.findByText("Invalid email");
  });

  it("city input validation check", async () => {
    render(<ContactForm />);
    const cityInput = screen.getByRole("textbox", {
      name: /city/i,
    });
    userEvent.type(cityInput, "123456789Sydney");
    await screen.findByText("Only letters are allowed for this field");
  });

  it("state input validation check", async () => {
    render(<ContactForm />);
    const stateInput = screen.getByRole("combobox", {
      name: /state/i,
    });
    userEvent.type(stateInput, "News South Wales 123456789");
    await screen.findByText("Only letters are allowed for this field");
  });

  it("postcode input validation check", async () => {
    render(<ContactForm />);
    const postCodeInput = screen.getByRole("textbox", {
      name: /postcode/i,
    });
    userEvent.type(postCodeInput, "postcode1234");
    await screen.findByText("Only numbers are allowed for this field");

    userEvent.clear(postCodeInput);
    userEvent.type(postCodeInput, "200");
    await screen.findByText(
      "Australian postcode must be 4 numbers, please try again"
    );

    userEvent.clear(postCodeInput);
    userEvent.type(postCodeInput, "20000000");
    await screen.findByText(
      "Australian postcode can not exceed 4 numbers, please try again"
    );
  });
});
