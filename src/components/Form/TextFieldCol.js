import { Col, Form } from "react-bootstrap";

const TextFieldCol = ({
  controlId,
  label,
  name,
  placeholder,
  value,
  onChange,
  isInvalid,
  type,
  errorsDisplay,
  onBlur,
}) => {
  return (
    <Col sm={6} md={4} lg={4}>
      <Form.Group controlId={controlId}>
        <Form.Label>{label}</Form.Label>
        <Form.Control
          name={name}
          type={type}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
          onBlur={onBlur}
          isInvalid={isInvalid}
        />
        <Form.Control.Feedback type="invalid">
          {errorsDisplay}
        </Form.Control.Feedback>
      </Form.Group>
    </Col>
  );
};

export default TextFieldCol;
