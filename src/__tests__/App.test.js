import { render, screen } from "@testing-library/react";
import App from "../App";

it("renders crunch accounting logo", () => {
  render(<App />);
  const logo = screen.getByRole("img", {
    name: /logo/i,
  });
  expect(logo).toBeInTheDocument();
});
